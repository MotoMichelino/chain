# CHAIN

CHAIN – Climate Change and Human Adaptation – aims at identifying human response and adaptation strategies to climate extreme events and climate-induced feedbacks over the past 500 years across Central Europe. 